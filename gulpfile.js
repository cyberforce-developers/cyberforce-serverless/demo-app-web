const gulp = require("gulp")
const sass = require("gulp-sass")
const postcss = require("gulp-postcss")
const rename = require('gulp-rename')
const autoprefixer = require("autoprefixer")
const cssnano = require("cssnano")
const uglifycss = require('gulp-uglifycss')
const sourcemaps = require("gulp-sourcemaps")

const ENV = process.env.ENV
if (!ENV) {
  throw new Error('ENV is not defined!')
}

const styles = {}

const common = {
  watch: function (watchPath, buildFn) {
    buildFn()
    gulp.watch(watchPath, buildFn)
  },
  copy: function (sourcePath, destPath) {
    return gulp
    .src(sourcePath)
    .pipe(gulp.dest(destPath))
  }
}

styles.build = function (sourcePath, destPath) {
  return gulp
    .src(sourcePath)
    .pipe(sass())
    .on("error", sass.logError)
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(sourcemaps.write())
    .pipe(uglifycss({
      "maxLineLen": 80,
      "uglyComments": true
    }))
    .pipe(rename({
      suffix: '.min',
      basename: 'main'
    }))
    .pipe(gulp.dest(destPath))
}

const paths = {
  public: {
    source: "./src/assets/public/**/*",
    dest: "./build/" + ENV
  },
  styles: {
    watch: "./src/assets/styles/**/*.scss",
    source: "./src/assets/styles/root.scss",
    dest: "./build/" + ENV + '/styles'
  }
}

exports.styles_build = () => styles.build(paths.styles.source, paths.styles.dest)

exports.styles_watch = () => common.watch(paths.styles.watch, () => styles.build(paths.styles.source, paths.styles.dest))

exports.assets_copy = () => common.copy(paths.public.source, paths.public.dest)