module.exports = {
  roots: ["./test"],
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },
  reporters: [
    "default"
  ],
  testMatch: [
    "<rootDir>/test/components/**/*.{spec,test}.{js,jsx,ts,tsx}",
    "<rootDir>/test/e2e/**/*.{spec,test}.{js,jsx,ts,tsx}"
  ],
  // runner: "jest-electron/runner",
  // testEnvironment: "jest-electron/environment",
  runner: '@jest-runner/electron',
  testEnvironment: '@jest-runner/electron/environment',
  collectCoverage: true,
  collectCoverageFrom: ["src/**/*.{js,jsx,ts}"],
  testPathIgnorePatterns: ["src/config"],
  coveragePathIgnorePatterns: [
    "/node_modules",
    "src/config",
    "coverage",
    ".vscode",
    "gulpfile.js",
    "webpack.js",
  ],
  setupFiles: [
    "react-app-polyfill/jsdom"
  ],
  setupFilesAfterEnv: [
    "<rootDir>/test/support/setupTests.js"
  ],
  coverageThreshold: {
    global: {
      branches: 10,
      functions: 10,
      lines: 10,
      statements: -10
    }
  }
}
