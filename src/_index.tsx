import { Provider } from 'mobx-react'
import { BrowserRouter } from 'react-router-dom'
import * as React from 'react'
import * as ReactDOM from 'react-dom'

import { Client } from '@cyberforce/essentials'

import { plugins } from './exports'
import { appRoutes } from './routes'
import config from './config'

const version = require('../package.json').version

import { IAppInitialProps } from './intf/IAppProps'

const renderFn = config.env === 'development'
  ? ReactDOM.render
  : ReactDOM.hydrate

renderFn(
  <Provider {...plugins}>
    <BrowserRouter>
      <Client.Bootstrap
        router="hash"
        appProps={{
          version,
          routes: appRoutes,
          config
        } as IAppInitialProps}
        routes={appRoutes}
      />
    </BrowserRouter>
  </Provider>, document.getElementById('app') as HTMLElement)
