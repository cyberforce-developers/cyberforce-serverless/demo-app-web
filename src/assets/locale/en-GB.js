export default {
  pages: {
    sectionIndex: {},
    sectionAbout: {},
    sectionDemo: {},
    sectionContact: {},
    pageError: {}
  },
  buttons: {
    btLang: 'Cambia per italiano'
  }
}