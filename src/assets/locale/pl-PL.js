export default {
  pages: {
    welcome: {
      title: 'Benvenuti sul nostro sito web',
      headline: 'Questo testo è stato aggiunto nel un traduzione file',
      flag: 'it'
    },
    title: 'Questo è un dimmer',
    description: 'Sembra che ci non hai creduto che la lingua si cambia dopo un click... Ma niente.'
  },
  buttons: {
    btLang: 'Change to English'
  }
}