import * as React from 'react'
import * as _ from 'lodash'
import { inject } from 'mobx-react'
import { Props } from '@cyberforce/essentials'
import { IAppProps } from '../intf/IAppProps'

@inject('prismic')
class App extends React.Component<IAppProps, {}> {

  public componentDidMount(): void {
    this.props.prismic.fetch(Props.PrismicProjectSchemas.SinglePageApplication)
  }

  public render(): JSX.Element {
    const newProps = {...this.props}
    delete newProps.children
    return React.cloneElement(this.props.children as React.ReactElement<any>, newProps as IAppProps)
  }

}

export default App
