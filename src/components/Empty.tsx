import * as React from 'react'

class Empty extends React.Component<{}, {}> {

  public render(): JSX.Element {
    return <span />
  }
}

export default Empty
