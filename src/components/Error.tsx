import * as React from 'react'

class Error extends React.Component<{}, {}> {

  public render(): JSX.Element {
    return <div className="Error">404 error</div>
  }
}

export default Error
