import * as React from 'react'
import { observer, inject } from 'mobx-react'
import { Client } from '@cyberforce/essentials'
import { Grommet } from 'grommet'

import Header from '../sections/Header'
import PrismicComponents from '../../props/PrismicComponents'
import PrismicSlices from '../../props/PrismicSlices'

import { IAppProps } from '../../intf/IAppProps'

const { PrismicText, PrismicImage, PrismicRichText } = Client.Helpers.PrismicToReact

@inject('prismic')
@observer

export default class MasterLayout extends React.Component<IAppProps, {}> {

  public render(): JSX.Element {
    return (
      <Grommet plain>
        <header className="App__Header">
          <Header />
          <PrismicImage {...this.props.prismic.component(
            PrismicSlices.Header,
            PrismicComponents.HeaderLogo
          )}/>
          <PrismicText {...this.props.prismic.component(
            PrismicSlices.Masthead,
            PrismicComponents.MastheadHeadlineText
          )}/>
          <PrismicRichText {...this.props.prismic.component(
            PrismicSlices.Body1,
            PrismicComponents.Body1SectionBody
          )}/>
        </header>
      </Grommet>
    )
  }

  // Missing children rendering

  private renderChildren = (): React.ReactElement => {
    const newProps = {...this.props}
    return React.cloneElement(this.props.children as React.ReactElement<any>, newProps)
  }

}