import * as React from 'react'
import { Box, Grid, Image, Header, Button, Menu } from 'grommet'
import * as Icons from 'grommet-icons'

import { NavHashLink as NavLink } from 'react-router-hash-link'

export default class MasterLayout extends React.Component<{}, {}> {

  public render(): JSX.Element {
    return (
      <Grid
        areas={[
          { name: 'logo', start: [0, 0], end: [0, 0] },
          { name: 'nav', start: [1, 0], end: [1, 0] }
        ]}
        columns={['small', 'flex']}
        rows={['small']}
        gap="small"
      >
        <Box gridArea="logo">
          <Image fit="contain" src="/images/cyberforce_logo.png" />
          <Header background="brand">
  <Button icon={<Icons.Home />} hoverIndicator />
  <Menu label="account" items={[{ label: 'logout' }]} />
</Header>
        </Box>
        <Box gridArea="nav">
          <nav className="App__Header__Nav">
              <ul>
                <li><NavLink smooth={true} to="/" exact={true} activeClassName="selected">Home</NavLink></li>
                <li><NavLink smooth={true} to="/about" activeClassName="selected">About</NavLink></li>
                <li><NavLink smooth={true} to="/demo" activeClassName="selected">Demo</NavLink></li>
                <li><NavLink smooth={true} to="/contact" activeClassName="selected">Contact</NavLink></li>
              </ul>
            </nav>
        </Box>
      </Grid>
    )
  }
}