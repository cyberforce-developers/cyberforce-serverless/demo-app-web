import { IAppConfig } from './intf/IAppConfig'

const env = process.env.ENV

if (!env) {
  throw new Error('Environment is not defined')
}

const baseUrl = (env === 'development')
  ? 'http://localhost'
  : 'https://cyberforce-demo-app-web.co.uk'

const compression = (env !== 'development') ? true : false

const config: IAppConfig = {
  appName: 'demo-app-web',
  env,
  baseUrl,
  options: {
    compression
  },
  plugins: {
    prismic: {
      url: 'https://acceleratetutors.cdn.prismic.io/api/v2',
      readOnlyApiKey: 'MC5YcUI4b1JBQUFDTUFRN090.77-977-9Ru-_ve-_ve-_vSvvv73vv73vv73vv70pbDTvv71BTO-_ve-_ve-_vVPvv73vv70M77-977-977-977-9FGlXGw'
    }
  }
}

export default config
