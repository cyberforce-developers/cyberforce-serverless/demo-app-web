import { Client, Props } from '@cyberforce/essentials'
import config from './config'
import { apiRoutes } from './routes'

const locale = new Client.Plugins.Locale()
const Languages = Props.Languages

locale.setTranslation(
  Languages.english,
  require('./assets/locale/en-GB').default
)

locale.setTranslation(
  Languages.polish,
  require('./assets/locale/pl-PL').default
)

locale.setLanguage(Props.Languages.english)

export const agent = new Client.Helpers.Agent(apiRoutes, {
  baseURL: config.baseUrl
})

const { url, readOnlyApiKey } = config.plugins.prismic
const prismic = new Client.Plugins.Prismic({
  url,
  token: readOnlyApiKey
})

export const plugins = {
  bucket: new Client.Plugins.Bucket(),
  prismic,
  locale
}