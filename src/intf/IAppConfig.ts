import { IBaseAppConfig } from '@cyberforce/essentials'

export interface IAppConfig extends IBaseAppConfig {
  plugins: {
    prismic: {
      url: string
      readOnlyApiKey: string
    }
  }
}