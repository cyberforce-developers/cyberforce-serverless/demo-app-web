/*
  Manually change it when adding a new localStore in routes and new Plugin
*/

import { IBaseAppStores } from '@cyberforce/essentials'
import AppBucket from '@cyberforce/essentials/src/client/plugins/bucket'
import PrismicPlugin from '@cyberforce/essentials/src/client/plugins/prismic'
import LocalePlugin from '@cyberforce/essentials/src/client/plugins/locale'

export interface IAppPlugins extends IBaseAppStores {
  bucket: AppBucket
  prismic: PrismicPlugin
  locale: LocalePlugin
}
