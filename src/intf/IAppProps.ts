import { IAppConfig } from './IAppConfig'
import { IAppRoute } from './IAppRoute'
import { IAppPlugins } from './IAppPlugins'

import { IBaseAppProps } from '@cyberforce/essentials'

export interface IAppInitialProps extends IBaseAppProps {
  config: IAppConfig
  routes: IAppRoute[]
}

export interface IAppBootstrappedProps extends IAppInitialProps {
  route: IAppRoute
}

export type IAppProps = IAppPlugins & IAppBootstrappedProps

export interface IAppHOCProps extends IAppProps {
  children: any
}
