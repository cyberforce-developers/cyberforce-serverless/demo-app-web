enum PrismicComponents {
  HeaderLogo = 'header_logo',
  MastheadPageTitle = 'masthead_page_title',
  MastheadHeadlineText = 'masthead_headline_text',
  MastheadBanner = 'masthead_banner',
  Body1SectionTitle = 'section_title',
  Body1SectionBody = 'section_body'
}

export default PrismicComponents
