enum PrismicSlices {
  Header = 'header',
  Masthead = 'masthead',
  Body1 = 'body_section_1'
}

export default PrismicSlices
