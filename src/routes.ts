import Error from './components/Error'
import Empty from './components/Empty'
import Master from './components/layouts/Master'

import App from './components/App'

import { IAppRoute } from './intf/IAppRoute'
import { IApiRoute } from './intf/IApiRoute'

export enum RouteSlugs {
  prismicContentAccess = 'prismicContentAccess'
}

export const appRoutes: IAppRoute[] = [
  {
    label: 'sectionIndex',
    uri: '/',
    exact: true,
    container: App,
    layout: Master,
    component: Empty,
  },
  {
    label: 'sectionAbout',
    uri: '/about',
    exact: true,
    container: App,
    layout: Master,
    component: Empty,
  },
  {
    label: 'sectionDemo',
    uri: '/demo',
    exact: true,
    container: App,
    layout: Master,
    component: Empty,
  },
  {
    label: 'sectionContact',
    uri: '/contact',
    exact: true,
    container: App,
    layout: Master,
    component: Empty,
  },
  {
    label: 'pageError',
    component: Error
  }
]

export const apiRoutes: IApiRoute[] = []
