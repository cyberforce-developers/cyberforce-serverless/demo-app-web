import * as React from 'react'
import { render } from '@testing-library/react'
import Error from '../../src/components/Error'

beforeAll(() => {
  require('electron').remote.getCurrentWindow().show()
})
test('renders error div', () => {
  const { container } = render(<Error />)
  const block = container.querySelector('div')
  expect(block).toBeInTheDocument()
})

test('error div contains a text', () => {
  const { queryByText } = render(<Error />)
  const textBlock = queryByText('404 error')
  expect(textBlock).toBeInTheDocument()
})