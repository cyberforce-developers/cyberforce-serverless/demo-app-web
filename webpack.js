var path = require('path')
const webpack = require('webpack')
const _ = require('lodash')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlReplaceWebpackPlugin = require('html-replace-webpack-plugin')
const CompressionPlugin = require('compression-webpack-plugin')
const BrotliPlugin = require('brotli-webpack-plugin')
const generate = require('nanoid-generate')

module.exports = env => {

  const realm = env.ENV

  console.log('\n\n webpack: webpack.js')
  console.log(` ENV: ${realm}\n\n`)
  const realms = {
    development: {
      devtool: 'inline-source-map',
      devServer: {
        contentBase: path.join(__dirname, 'build', realm),
        compress: true,
        hot: true,
        port: 3000
      },
      plugins: []
    },
    production: {
      optimization: {
        minimize: true,
        moduleIds: 'hashed'
      },
      plugins: [
        new CompressionPlugin({
          filename: '[path].gz[query]',
          algorithm: 'gzip',
          test: /\.js$|\.css$|\.html$/,
          threshold: 10240,
          minRatio: 0.7
        }),
        new BrotliPlugin({
          filename: '[path].br[query]',
          test: /\.js$|\.css$|\.html$/,
          threshold: 10240,
          minRatio: 0.7
        })
      ]
    }
  }

  const commonPlugins = [
    new HtmlWebpackPlugin({
      hash: true,
      template: path.resolve(__dirname, "src", "assets", 'index.html'),
      filename: path.resolve(__dirname, "build", realm, 'index.html')
    }),
    new webpack.DefinePlugin({
      'process.env.ENV': JSON.stringify(realm)
    }),
    new HtmlReplaceWebpackPlugin([
      {
        pattern: '_BUILD_',
        replacement: generate.numbers(8).toString()
      }
    ])
  ]

  return _.merge(realms[realm], {
    mode: realm,
    entry: [
      require.resolve(path.resolve(__dirname, "src", "assets", 'polyfills.js')),
      require.resolve(path.resolve(__dirname, "src", '_index.tsx'))
    ],
    output: {
      publicPath: '/app',
      path: `/${path.resolve(__dirname)}/build/${realm}/app`,
      filename: "app.js"
    },
    resolve: {
      extensions: [".ts", ".tsx", ".js"]
    },
    module: {
      rules: [
        {
          test: /\.(ts|tsx)?$/,
          loader: "ts-loader",
          exclude: /node_modules/
        },
        {
          test: /\.css$/i,
          use: ['style-loader', 'css-loader'],
        }
      ]
    },
    stats: { 
      children: false,
      warnings: false
    },
    plugins: [
      ...commonPlugins,
      ...realms[realm].plugins
    ]
  })
}
